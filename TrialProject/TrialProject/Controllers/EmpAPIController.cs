﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrialProject.Models;

namespace TrialProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpAPIController : ControllerBase
    {
        EmployeesystemContext _context;

        public EmpAPIController(EmployeesystemContext context)
        {
            _context = context;
        }

        public IActionResult GetInfo()
        {
            List<ViewModel> viewModels = new List<ViewModel>();

            var employees = _context.Employee.ToList();

            for (int i = 1; i < employees.Count; i++)
            {
                string deptName = _context.Department.Find(employees[i].DeptId).DeptName;
                viewModels.Add(new ViewModel
                {
                    UserId = employees[i].UserId,
                    ImagePath = employees[i].ImagePath,
                    EmpFirstName = employees[i].EmpFirstName,
                    EmpMidName = employees[i].EmpMidName,
                    EmpLastName = employees[i].EmpLastName,
                    Gender = employees[i].Gender,
                    Salary = employees[i].Salary,
                    Experience = employees[i].Experience,
                    DOB = employees[i].Dob,
                    DeptName = deptName
                });
            }

            return Ok(viewModels);
        }

        [HttpPost]
        public IActionResult DeleteInfo(ViewModel view)
        {
            if ( _context.Access.Find(view.UserId).IsAdmin != "true" )
            {
                _context.Access.Remove(_context.Access.Find(view.UserId));
                _context.Employee.Remove(_context.Employee.Find(view.UserId));
                _context.SaveChanges();
            }

            return Ok();
        }
    }
}