﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TrialProject.Models;
using System.Data;
using System.Web;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace TrialProject.Controllers
{
    public class ProfileController : Controller
    {
        private EmployeesystemContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProfileController(EmployeesystemContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Details()
        {
            int? ID = HttpContext.Session.GetInt32("User");
            using (var client = new HttpClient())
            {
                var view = new ViewModel();

                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("ProfileAPI?id="+ ID.ToString());

                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                    view = JsonConvert.DeserializeObject<ViewModel>(EmpResponse);
                }

                if (view == null)
                {
                    return new NoContentResult();
                }

                if (_context.Access.Find(view.UserId).IsAdmin == "true")
                {
                    HttpContext.Session.SetInt32("Admin", view.UserId);
                    return RedirectToAction("AdminAccess", "Admin");
                }

                if (view.EmpFirstName == null || view.EmpLastName == null)
                {
                    ViewBag.EmptyName = "Enter/Edit Your Details in Edit tab";
                }

                ViewData["fileLocation"] = "/UserImages/" + view.ImagePath;

                return View(view);
            }
        }

        public async Task<ActionResult> Edit()
        {
            ViewModel view = new ViewModel();

            int? id = HttpContext.Session.GetInt32("User");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("ProfileAPI?id=" + id.ToString());

                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                    view = JsonConvert.DeserializeObject<ViewModel>(EmpResponse);
                }

                if (view == null)
                {
                    return new NoContentResult();
                }

                ViewData["fileLocation"] = "/UserImages/" + view.ImagePath;
            }

            return View(view);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(ViewModel viewModel, IFormFile pic)
        {
            if (viewModel == null)
            {
                return NoContent();
            }

            if (CheckAlpha(viewModel.EmpFirstName) == false)
            {
                ViewBag.FirstNameError = "No special symbols/ numbers/ whitspaces allowed";
                return View(viewModel);
            }

            if (viewModel.EmpMidName != null)
            {
                if (CheckAlpha(viewModel.EmpMidName) == false)
                {
                    ViewBag.MidNameError = "No special symbols/ numbers/ whitespaces allowed";
                    return View(viewModel);
                }
            }

            if (CheckAlpha(viewModel.EmpLastName) == false)
            {
                ViewBag.LastNameError = "No special symbols/ numbers/ whitespaces allowed";
                return View(viewModel);
            }

            DateTime current = DateTime.Now;

            if ((current.Year - viewModel.DOB.Year) < 18)
            {
                ViewBag.InvalidDate = "Employee must be al least 18 years of age!";
                return View(viewModel);
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res;

                if (pic != null)
                {
                    var filename = Path.Combine(_hostingEnvironment.WebRootPath, "UserImages", Path.GetFileName(pic.FileName));
                    pic.CopyTo(new FileStream(filename, FileMode.Create));
                    viewModel.ImagePath = pic.FileName;
                }

                Res = await client.PostAsJsonAsync<ViewModel>("ProfileAPI", viewModel);

                if (Res.IsSuccessStatusCode)
                {
                    ViewData["fileLocation"] = "/UserImages/" + viewModel.ImagePath;
                    return RedirectToAction("Details", new { ID = viewModel.UserId });
                }
            }

            return RedirectToAction("Details");
        }

        public bool CheckAlpha(string S)
        {
            if (S == null)
                return false;

            for (int i = 0; i < S.Length; i++)
            {
                if( char.IsDigit(Convert.ToChar(S.Substring(i,1))) 
                    || char.IsPunctuation(Convert.ToChar(S.Substring(i, 1))) 
                    || char.IsSymbol(Convert.ToChar(S.Substring(i, 1))) 
                    || char.IsSeparator(Convert.ToChar(S.Substring(i, 1)))
                    || char.IsWhiteSpace(Convert.ToChar(S.Substring(i, 1))) )
                    return false;
            }

            return true;
        }
    }
}