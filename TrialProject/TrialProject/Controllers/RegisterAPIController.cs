﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.DataProtection;
using TrialProject.Models;
using System.Net.Mail;
using System.Text;

namespace TrialProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterAPIController : ControllerBase, ICipherService
    {
        EmployeesystemContext _context;
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private const string key = "Mxo26SnC91m6D3l0";

        public RegisterAPIController(EmployeesystemContext context, IDataProtectionProvider dataProtectionProvider)
        {
            _dataProtectionProvider = dataProtectionProvider;
            _context = context;
        }

        [HttpPost]
        public IActionResult Post(Registration registration)
        {
            _context.Employee.Add(new Employee()
            {
                UserId = 1 + _context.Employee.Last().UserId,
                UserName = registration.UserName,
                Email = registration.Email,
                DeptId = 0,
                Dob = Convert.ToDateTime("31-12-1999"),
                ImagePath="Default.jpg",
                Pass = encryptTo(registration.Password)
            });

            _context.Access.Add(new Access() { UserId = 1 + _context.Employee.Last().UserId, IsAdmin = "false" });

            _context.SaveChanges();

            int NewUserID = _context.Employee.Last().UserId;

            const string V = "yesashish123456@gmail.com";
            MailMessage message = new MailMessage(V, registration.Email);
            StringBuilder mailbody = new StringBuilder();

            mailbody.Append("CONGRATULATION. "+registration.UserName.ToUpper());
            mailbody.Append("<br/>");
            mailbody.Append("Registration complete! ");
            mailbody.Append("Your User-ID is " + NewUserID);
            string OGPassword = registration.Password;
            string halfPassword = OGPassword.Substring(0, 2) +
                "*******" + OGPassword.Substring((OGPassword.Length) - 1, 1);
            mailbody.Append(" and your password is - " + halfPassword);
            message.IsBodyHtml = true;
            message.Body = mailbody.ToString();
            message.Subject = "reset password";
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new System.Net.NetworkCredential("yesashish123456@gmail.com", "Aj@12345"),
                EnableSsl = true
            };
            smtpClient.Send(message);

            return Ok();
        }

        public string encryptTo(string S)
        {
            var encrypt = _dataProtectionProvider.CreateProtector(key);

            return encrypt.Protect(S);
        }

        public string DecryptProtectedData(string encryptedText, string environmentVariableName)
        {
            throw new NotImplementedException();
        }
    }
}