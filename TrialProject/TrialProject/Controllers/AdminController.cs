﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TrialProject.Models;
using TrialProject.ViewModels;

namespace TrialProject.Controllers
{
    public class AdminController : Controller
    {
        EmployeesystemContext _context;
        IHostingEnvironment _hostingEnvironment;

        public AdminController(EmployeesystemContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> AdminAccess()
        {
            int? ID = HttpContext.Session.GetInt32("Admin");

            using (var client = new HttpClient())
            {
                var view = new ViewModel();

                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("ProfileAPI?id=" + ID.ToString());

                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                    view = JsonConvert.DeserializeObject<ViewModel>(EmpResponse);
                }

                if (view == null)
                {
                    return new NoContentResult();
                }

                if (view.EmpFirstName == null || view.EmpLastName == null)
                {
                    ViewBag.EmptyName = "Enter/Edit Your Details in Edit tab";
                }

                ViewData["fileLocation"] = "/UserImages/" + view.ImagePath;

                return View(view);
            }
        }

        public async Task<ActionResult> Edit()
        {
            ViewModel view = new ViewModel();

            int? id = HttpContext.Session.GetInt32("Admin");

            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("https://localhost:44319/api/");
                    client.DefaultRequestHeaders.Clear();
                    client.CancelPendingRequests();
                    client.Timeout = TimeSpan.FromMinutes(5);

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage Res = await client.GetAsync("ProfileAPI?id=" + id.ToString());

                    if (Res.IsSuccessStatusCode)
                    {
                        var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                        view = JsonConvert.DeserializeObject<ViewModel>(EmpResponse);
                    }

                    if (view == null)
                    {
                        return new NoContentResult();
                    }

                    List<Department> departments = new List<Department>();
                    departments = _context.Department.ToList();
                    ViewBag.listofDepartments = departments;

                    ViewData["fileLocation"] = "/UserImages/" + view.ImagePath;
                }
                catch (Exception e) { Console.WriteLine(e.StackTrace); }
            }
            return View(view);
        }



        [HttpPost]
        public async Task<ActionResult> Edit(ViewModel viewModel, IFormFile pic)
        {
            if (viewModel == null)
            {
                return NoContent();
            }

            if (CheckAlpha(viewModel.EmpFirstName) == false)
            {
                ViewBag.FirstNameError = "No special symbols/ numbers/ whitspaces allowed";
                return View(viewModel);
            }

            if (viewModel.EmpMidName != null)
            {
                if (CheckAlpha(viewModel.EmpMidName) == false)
                {
                    ViewBag.MidNameError = "No special symbols/ numbers/ whitespaces allowed";
                    return View(viewModel);
                }
            }

            if (CheckAlpha(viewModel.EmpLastName) == false)
            {
                ViewBag.LastNameError = "No special symbols/ numbers/ whitespaces allowed";
                return View(viewModel);
            }

            DateTime current = DateTime.Now;

            if ((current.Year - viewModel.DOB.Year) < 18)
            {
                ViewBag.InvalidDate = "Employee must be al least 18 years of age!";
                return View(viewModel);
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                
                if (pic != null)
                {
                    var filename = Path.Combine(_hostingEnvironment.WebRootPath, "UserImages", Path.GetFileName(pic.FileName));
                    pic.CopyTo(new FileStream(filename, FileMode.Create));
                    viewModel.ImagePath = pic.FileName;
                }

                HttpResponseMessage Res = await client.PostAsJsonAsync<ViewModel>("ProfileAPI", viewModel);

                if (Res.IsSuccessStatusCode)
                {
                    ViewData["fileLocation"] = "/UserImages/" + viewModel.ImagePath;
                    return RedirectToAction("AdminAccess");
                }
            }

            return RedirectToAction("AdminAccess");
        }

        public async Task<IActionResult> Data()
        {
            List<ViewModel> employees = new List<ViewModel>();

            int? id = HttpContext.Session.GetInt32("Admin");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("EmpAPI");

                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                   employees = JsonConvert.DeserializeObject<List<ViewModel>>(EmpResponse);
                }

                if (employees == null)
                {
                    return NoContent();
                }
            }
            ViewData["fileLocation"] = "/UserImages/" + _context.Employee.Find(id).ImagePath;
            return View(employees);
        }

        public async Task<IActionResult> DeptData()
        {
            var departments = new List<Department>();

            int? id = HttpContext.Session.GetInt32("Admin");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("DeptAPI");

                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                    departments = JsonConvert.DeserializeObject<List<Department>>(EmpResponse);
                }

                if (departments == null)
                {
                    return NoContent();
                }
            }

            ViewData["fileLocation"] = "/UserImages/" + _context.Employee.Find(id).ImagePath;

            return View(departments);
        }

        public async Task<IActionResult> EditEmp(int id)
        {
            ViewModel view = new ViewModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("ProfileAPI?id=" + id.ToString());

                if (Res.IsSuccessStatusCode)
                {
                    var EmpResponse = Res.Content.ReadAsStringAsync().Result;

                    view = JsonConvert.DeserializeObject<ViewModel>(EmpResponse);
                }

                if (view == null)
                {
                    return new NoContentResult();
                }

                ViewData["fileLocation"] = "/UserImages/" + view.ImagePath;
            }

            List<Department> departments = new List<Department>();
            departments = _context.Department.ToList();
            ViewBag.listofDepartments = departments;
            ViewData["fileLocation"] = "/UserImages/" + _context.Employee.Find(HttpContext.Session.GetInt32("Admin")).ImagePath;

            return View(view);
        }

        [HttpPost]
        public async Task<ActionResult> EditEmp(ViewModel viewModel)
        {
            if (viewModel == null)
            {
                return NoContent();
            }

            if (CheckAlpha(viewModel.EmpFirstName) == false)
            {
                ViewBag.FirstNameError = "No special symbols/ numbers/ whitspaces allowed";
                return View(viewModel);
            }

            if (viewModel.EmpMidName != null)
            {
                if (CheckAlpha(viewModel.EmpMidName) == false)
                {
                    ViewBag.MidNameError = "No special symbols/ numbers/ whitespaces allowed";
                    return View(viewModel);
                }
            }

            if (CheckAlpha(viewModel.EmpLastName) == false)
            {
                ViewBag.LastNameError = "No special symbols/ numbers/ whitespaces allowed";
                return View(viewModel);
            }

            DateTime current = DateTime.Now;

            if ((current.Year - viewModel.DOB.Year) < 18)
            {
                ViewBag.InvalidDate = "Employee must be al least 18 years of age!";
                return View(viewModel);
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.PostAsJsonAsync<ViewModel>("ProfileAPI", viewModel);

                if (Res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Data");
                }
            }

            return NoContent();
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NoContent();
            }

            Employee employee = _context.Employee.Find(id);

            if (employee == null)
            {
                return NoContent();
            }

            ViewModel view = new ViewModel()
            {
                UserId=employee.UserId,
                EmpFirstName = employee.EmpFirstName,
                EmpLastName = employee.EmpLastName
            };

            ViewData["fileLocation"] = "/UserImages/" + _context.Employee.Find(HttpContext.Session.GetInt32("Admin")).ImagePath;

            return View(view);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(ViewModel viewModel)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.PostAsJsonAsync<ViewModel>("EmpAPI", viewModel);

                if (Res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Data");
                }
            }

            return NoContent();
        }

        public IActionResult EditDept(int? id)
        {
            if (id == null)
            {
                return NoContent();
            }

            var department = new Department()
            {
                DeptId = _context.Department.Find(id).DeptId,
                DeptName = _context.Department.Find(id).DeptName
            };

            ViewData["fileLocation"] = "/UserImages/" + _context.Employee.Find(HttpContext.Session.GetInt32("Admin")).ImagePath;

            return View(department);
        }

        [HttpPost]
        public IActionResult EditDept(Department dept)
        {
            if (dept == null)
            {
                return NoContent();
            }

            Department department = _context.Department.Find(dept.DeptId);
            Department check = _context.Department.FirstOrDefault(d => d.DeptName == dept.DeptName);

            if (check == null)
            {
                department.DeptName = dept.DeptName;
                _context.SaveChanges();
                return RedirectToAction("DeptData");
            }
            else
            {
                ViewBag.Exists = "Department already exists.";
                ViewData["fileLocation"] = "/UserImages/" + _context.Employee.Find(HttpContext.Session.GetInt32("Admin")).ImagePath;
                return View(dept);
            }
        }

        public IActionResult DeleteDept(int id)
        {
            ViewData["fileLocation"] = "/UserImages/" + _context.Employee.Find(HttpContext.Session.GetInt32("Admin")).ImagePath;

            return View(_context.Department.Find(id));
        }

        [HttpPost]
        public async Task<IActionResult> DeleteDept(Department dept)
        {
            //Department department = _context.Department.Find(dept.DeptId);
            //var employees = _context.Employee.FromSql("select * from Employee").Where(d => d.DeptId == dept.DeptId);

            //foreach (var employee in employees)
            //{
            //    Access access = _context.Access.Find(employee.UserId);

            //    _context.Access.Remove(access);
            //    _context.SaveChanges();

            //    _context.Employee.Remove(employee);
            //    _context.SaveChanges();
            //}

            //_context.Department.Remove(department);
            //_context.SaveChanges();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.PostAsJsonAsync<Department>("DeptAPI", dept);

                if (Res.IsSuccessStatusCode)
                {
                    return RedirectToAction("DeptData");
                }
            }

                return RedirectToAction("DeptData");
        }

        public bool CheckAlpha(string S)
        {
            if (S == null)
                return false;

            for (int i = 0; i < S.Length; i++)
            {
                if (char.IsDigit(Convert.ToChar(S.Substring(i, 1)))
                    || char.IsPunctuation(Convert.ToChar(S.Substring(i, 1)))
                    || char.IsSymbol(Convert.ToChar(S.Substring(i, 1)))
                    || char.IsSeparator(Convert.ToChar(S.Substring(i, 1)))
                    || char.IsWhiteSpace(Convert.ToChar(S.Substring(i, 1))))
                    return false;
            }

            return true;
        }
    }
}