﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TrialProject.Models;

namespace TrialProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeptAPIController : ControllerBase
    {
        EmployeesystemContext _context;

        public DeptAPIController(EmployeesystemContext context)
        {
            _context = context;
        }

        public ActionResult GetInfo()
        {
            return Ok(_context.Department.ToList());
        }

        [HttpPost]
        public IActionResult DeleteInfo(Department dept)
        {
            Department department = _context.Department.Find(dept.DeptId);
            var employees = _context.Employee.FromSql("select * from Employee").Where(d => d.DeptId == dept.DeptId);

            foreach (var employee in employees)
            {
                Access access = _context.Access.Find(employee.UserId);

                _context.Access.Remove(access);
                _context.SaveChanges();

                _context.Employee.Remove(employee);
                _context.SaveChanges();
            }

            _context.Department.Remove(department);
            _context.SaveChanges();

            return Ok();
        }
    }
}