﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrialProject.Models;

namespace TrialProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfileAPIController : ControllerBase
    {
        EmployeesystemContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ProfileAPIController(EmployeesystemContext context,IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        public ActionResult GetInfo(int id)
        {
            Employee employee = _context.Employee.Find(id);

            var view = new ViewModel
            {
                UserId = employee.UserId,
                EmpFirstName = employee.EmpFirstName,
                EmpMidName = employee.EmpMidName,
                EmpLastName = employee.EmpLastName,
                Gender = employee.Gender,
                Salary = employee.Salary,
                Experience = employee.Experience,
                DOB = employee.Dob,
                DeptName = _context.Department.Find(employee.DeptId).DeptName,
                ImagePath = employee.ImagePath
            };

            return Ok(view);
        }

        [HttpPost]
        public IActionResult PutInfo(ViewModel viewModel)
        {
            Employee employee = _context.Employee.Find(viewModel.UserId);

            employee.EmpFirstName = viewModel.EmpFirstName;
            employee.EmpMidName = viewModel.EmpMidName;
            employee.EmpLastName = viewModel.EmpLastName;
            employee.Gender = viewModel.Gender;
            employee.Salary = viewModel.Salary;
            employee.Experience = viewModel.Experience;
            employee.Dob = viewModel.DOB;
            employee.DeptId = _context.Department.FirstOrDefault(m => m.DeptName == viewModel.DeptName).DeptId;
            employee.ImagePath = viewModel.ImagePath;
            if (_context.Access.Find(employee.UserId).IsAdmin == "true")
                employee.DeptId = _context.Department.FirstOrDefault(m => m.DeptName == viewModel.DeptName).DeptId;

            _context.SaveChanges();

            return Ok(viewModel);
        }
    }

    public interface IHttpActionResult
    {
        Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken);
    }
}