﻿using TrialProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
//using ReflectionIT.Mvc.Paging;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using System.Net.Http;
using System.Net.Http.Headers;

namespace FirstProject.Controllers
{
    public interface ICipherService
    {
        string DecryptProtectedData(string encryptedText, string environmentVariableName);
    }

    public class RegisterController : Controller
    {
        private EmployeesystemContext _context;
        public RegisterController(EmployeesystemContext context)
        {
            _context = context;
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Registration registration)
        {
            Employee checkUserName = _context.Employee.SingleOrDefault(
                 m => m.UserName == registration.UserName);
            if (checkUserName == null)
            {
                Employee checkEmail = _context.Employee.SingleOrDefault(
                m => m.Email == registration.Email);

                if (checkEmail == null)
                {
                    if (ModelState.IsValid)
                    {
                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("https://localhost:44319/api/");
                            client.DefaultRequestHeaders.Clear();
                            client.Timeout = TimeSpan.FromMinutes(5);

                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            HttpResponseMessage Res = await client.PostAsJsonAsync<Registration>("RegisterAPI", registration);
                            if (Res.IsSuccessStatusCode)
                            {
                                return RedirectToAction("Success", new { id = _context.Employee.Last().UserId });
                            }
                        }
                    }
                }
                else
                {
                    ViewBag.ErrorEmail = "Email already exists";
                }
            }
            else
            {
                ViewBag.ErrorUserName = "UserName already exists";
                return View("Create");
            }


            return View(registration);
        }

        public IActionResult Success(int? id)
        {
            return View(_context.Employee.Find(id));
        }
    }
}



