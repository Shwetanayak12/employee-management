﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using TrialProject.Models;
using TrialProject.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace TrialProject.Controllers
{
    public interface ICipherService
    {
        string DecryptProtectedData(string encryptedText, string environmentVariableName);
    }

    public class LoginController : Controller, ICipherService
    {
        EmployeesystemContext _context;
        private readonly IDataProtectionProvider _dataProtectionProvider;
        private const string key = "Mxo26SnC91m6D3l0";

        public LoginController(EmployeesystemContext context, IDataProtectionProvider dataProtectionProvider)
        {
            _context = context;
            _dataProtectionProvider = dataProtectionProvider;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Details(LoginModel view)
        {
            if (view == null)
            {
                return NoContent();
            }
            Employee employee;

            if (CheckEmailOrNot(view.IdOrEmail) == true)
            {
                employee = _context.Employee.FirstOrDefault(d => d.Email == view.IdOrEmail);
            }
            else
            {
                employee = _context.Employee.Find(Convert.ToInt32(view.IdOrEmail));
            }


            if (employee == null)
            {
                return NoContent();
            }

            if (decryptFrom(employee.Pass) == view.Password)
            {
                HttpContext.Session.SetInt32("User", employee.UserId);
                return RedirectToAction("Details", "Profile", new { ID = employee.UserId });

            }
            else
            {
                ViewBag.error = "Password or User-ID don't match";
                return View("Index");
            }
        }
        public IActionResult Forgotpassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Forgotpassword(Employee employee)
        {
            if (employee == null)
            {
                return NoContent();
            }

            Employee emp = _context.Employee.FirstOrDefault(e => e.Email == employee.Email);
            if (emp != null && emp.UserId == employee.UserId)
            {
                MailMessage message = new MailMessage("yesashish123456@gmail.com", emp.Email);
                StringBuilder mailbody = new StringBuilder();

                mailbody.Append("click the below link to enter the new password");
                mailbody.Append("<br/>");
                mailbody.Append("https://localhost:44319/Login/Repassword/" + employee.UserId);
                message.IsBodyHtml = true;
                message.Body = mailbody.ToString();
                message.Subject = "reset password";
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new System.Net.NetworkCredential("yesashish123456@gmail.com", "Aj@12345"),
                    EnableSsl = true
                };
                smtpClient.Send(message);
            }
            else
                ViewBag.Mismatch = "Email/User-ID does not exist";

            return View();

        }

        public IActionResult Repassword(int? id)
        {
            if (id == null)
            {
                return NoContent();
            }

            ConfirmPassword confirmPassword = new ConfirmPassword()
            {
                UserId = id
            };

            return View(confirmPassword);
        }

        [HttpPost]
        public IActionResult Repassword(ConfirmPassword confirmPassword)
        {
            if (confirmPassword == null)
            {
                return NoContent();
            }
            //var some=new
            Employee emp = _context.Employee.Find(confirmPassword.UserId);
            if (emp == null)
            {
                return NoContent();
            }


            if (confirmPassword.Password == confirmPassword.ConPass)
            {
                Employee password = _context.Employee.Find(confirmPassword.UserId);
                password.Pass = encryptTo(confirmPassword.Password);
                _context.SaveChanges();
                return View("Index");
            }
            else
            {
                ViewBag.Mismatch = "Passwords don't match!";
                return View(confirmPassword);
            }
        }

        public string decryptFrom(string S)
        {
            var decrypt = _dataProtectionProvider.CreateProtector(key);

            return decrypt.Unprotect(S);
        }

        public string encryptTo(string S)
        {
            var encrypt = _dataProtectionProvider.CreateProtector(key);

            return encrypt.Protect(S);
        }

        public string DecryptProtectedData(string encryptedText, string environmentVariableName)
        {
            throw new NotImplementedException();
        }

        public bool CheckEmailOrNot(string S)
        {
            if (S == null)
                return false;

            for (int i = 0; i < S.Length; i++)
            {
                if (char.IsLetter(Convert.ToChar(S.Substring(i, 1))))
                    return true;
            }

            return false;
        }
    }
}