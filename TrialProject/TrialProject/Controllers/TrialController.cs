﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TrialProject.Models;

namespace TrialProject.Controllers
{
    public class TrialController : Controller
    {
        EmployeesystemContext _context;
        //ProfileAPIController _api;

        public TrialController(EmployeesystemContext context/*ProfileAPIController api*/)
        {
            _context = context;
            //_api = api;
        }

        //public IActionResult Index()
        //{
        //    ViewModel emp = null;

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("http://localhost:44319/api/");
        //        var responseTask = client.PostAsJsonAsync("ProfileAPI",1);

        //        //responseTask.Wait();

        //        var result = responseTask.Result;

        //        if (result.IsSuccessStatusCode)
        //        {
        //            var readTask = result.Content.ReadAsAsync<ViewModel>();
        //            //readTask.Wait();

        //            emp = readTask.Result;
        //        }
        //        else
        //        {
        //            ModelState.AddModelError(string.Empty, "Server Error");
        //        }
        //    }

        //    return View(emp);
        //}

        //public ActionResult Index()
        //{
        //    ViewModel emp = null;

        //    HttpClient client= _api.
        //}

        //public async Task<ActionResult> Index()
        //{
        //    ViewModel emp = new ViewModel();

        //    using (var client = new HttpClient())
        //    {
        //        client.BaseAddress = new Uri("https://localhost:44319/api/");
        //        client.DefaultRequestHeaders.Clear();
        //        client.Timeout = TimeSpan.FromMinutes(5);

        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //        HttpResponseMessage Res = await client.PostAsJsonAsync("ProfileAPI",0);

        //        if (Res.IsSuccessStatusCode)
        //        {
        //            var EmpResponse = Res.Content.ReadAsStringAsync().Result;

        //            emp = JsonConvert.DeserializeObject<ViewModel>(EmpResponse);
        //        }
        //    }
        //    return View(emp);
        //}

        public async Task<ActionResult> Index()
        {
            List<ViewModel> view = new List<ViewModel>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("applicaiton/json"));

                HttpResponseMessage res = await client.GetAsync("ProfileAPI");
                if (res.IsSuccessStatusCode)
                {
                    var EmpResponse = res.Content.ReadAsStringAsync().Result;

                    view = JsonConvert.DeserializeObject<List<ViewModel>>(EmpResponse);
                }

                return View(view.Find(m=>m.UserId==0));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Create(Registration registration)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44319/api/");
                client.DefaultRequestHeaders.Clear();
                client.Timeout = TimeSpan.FromMinutes(5);

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage res = await client.PostAsJsonAsync<Registration>("ProfileAPI", registration);
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            return View();
        }

        public ActionResult Create()
        {
            return View();
        }
    }
}