﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TrialProject.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please provide UserId or Registered E-mail ID", AllowEmptyStrings = false)]
        public string IdOrEmail { get; set; }

        [Required(ErrorMessage = "Please provide Password", AllowEmptyStrings = false)]
        public string Password { get; set; }
    }
}
