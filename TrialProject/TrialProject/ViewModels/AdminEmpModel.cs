﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrialProject.Models;

namespace TrialProject.ViewModels
{
    public class AdminEmpModel
    {
        public int? AdminId { get; set; }
        public List<ViewModel> Employees { get; set; }
    }
}
