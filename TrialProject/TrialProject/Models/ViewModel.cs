﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TrialProject.Models
{
    public class ViewModel
    {
        public int UserId { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpMidName { get; set; }
        public string EmpLastName { get; set; }
        public string Gender { get; set; }
        public int? Salary { get; set; }
        public int? Experience { get; set; }

        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DOB { get; set; }
        public string DeptName { get; set; }

        public string ImagePath { get; set; }

        public List<Employee> Employees { get; set; }
    }
}
