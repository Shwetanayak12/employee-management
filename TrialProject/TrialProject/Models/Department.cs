﻿using System;
using System.Collections.Generic;

namespace TrialProject.Models
{
    public partial class Department
    {
        public Department()
        {
            Employee = new HashSet<Employee>();
        }

        public int DeptId { get; set; }
        public string DeptName { get; set; }

        public ICollection<Employee> Employee { get; set; }
    }
}
