﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace TrialProject.Models
{
    public class Registration
    {
        public int UserID { get; set; }

        [Required(ErrorMessage = "Please provide UserName", AllowEmptyStrings = false)]
        public string UserName { get; set; }

        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,3})$",
        ErrorMessage = "Please provide valid Email")]
        [Display(Name = "Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please provide Password", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [StringLength(16, MinimumLength = 5, ErrorMessage = "Password must be 5 char long.")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please provide Password", AllowEmptyStrings = false)]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        [StringLength(16, MinimumLength = 5, ErrorMessage = "Password must be 5 char long.")]
        [Compare("Password", ErrorMessage = "Confirm password does not match.")]
        [Display(Name = "Confirm Password")]
        public string ConPass { get; set; }
    }
}
