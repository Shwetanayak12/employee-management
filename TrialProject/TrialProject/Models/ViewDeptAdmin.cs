﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrialProject.Models
{
    public class ViewDeptAdmin
    {
        public int AdminId { get; set; }

        public int DeptId { get; set; }
        public string DeptName { get; set; }

        public List<Department> Departments { get; set; }
    }
}
