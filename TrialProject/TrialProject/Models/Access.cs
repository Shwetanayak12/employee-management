﻿using System;
using System.Collections.Generic;

namespace TrialProject.Models
{
    public partial class Access
    {
        public int UserId { get; set; }
        public string IsAdmin { get; set; }

        public Employee User { get; set; }
    }
}
