﻿using System;
using System.Collections.Generic;

namespace TrialProject.Models
{
    public partial class Employee
    {
        public int UserId { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpMidName { get; set; }
        public string EmpLastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public int? Salary { get; set; }
        public int? Experience { get; set; }
        public DateTime Dob { get; set; }
        public int? DeptId { get; set; }
        public string Pass { get; set; }
        public string ImagePath { get; set; }

        public Department Dept { get; set; }
        public Access Access { get; set; }
    }
}
