#pragma checksum "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2963796a0bffafede4b1b07fb717ea0b84c176d5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_Data), @"mvc.1.0.view", @"/Views/Admin/Data.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Admin/Data.cshtml", typeof(AspNetCore.Views_Admin_Data))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\_ViewImports.cshtml"
using TrialProject;

#line default
#line hidden
#line 2 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\_ViewImports.cshtml"
using TrialProject.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2963796a0bffafede4b1b07fb717ea0b84c176d5", @"/Views/Admin/Data.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"48811276c92b6709689023abd45a5c62a55e6a69", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin_Data : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<TrialProject.Models.ViewModelAdmin>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/StyleSheet.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("height", new global::Microsoft.AspNetCore.Html.HtmlString("40px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("width", new global::Microsoft.AspNetCore.Html.HtmlString("40px"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
  
    ViewData["Title"] = "Data";
    Layout = "~/Pages/EmployeeData.cshtml";

#line default
#line hidden
            BeginContext(87, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(145, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(269, 53, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "be7529e1a82f4fe1a013c42bb4ebd70b", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(322, 565, true);
            WriteLiteral(@"

<div class=""main"">
    <h2>Data</h2>

    <table id=""Employees"" class=""main"">
        <thead>
            <tr>
                <th>User ID</th>
                <th></th>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
                <th>Gender</th>
                <th>Salary</th>
                <th>Experience</th>
                <th>Date of Birth</th>
                <th>Department</th>
                <th>Edit|Delete</th>
            </tr>
        </thead>
        <tbody>
");
            EndContext();
#line 36 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
            BeginContext(944, 72, true);
            WriteLiteral("                <tr>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1017, 41, false);
#line 40 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.UserId));

#line default
#line hidden
            EndContext();
            BeginContext(1058, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1137, 68, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "7ef2d70080fc400ea3dea6ce96b052a1", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            BeginAddHtmlAttributeValues(__tagHelperExecutionContext, "src", 2, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            AddHtmlAttributeValue("", 1147, "~/images/", 1147, 9, true);
#line 43 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
AddHtmlAttributeValue("", 1156, item.ImagePathEmp, 1156, 18, false);

#line default
#line hidden
            EndAddHtmlAttributeValues(__tagHelperExecutionContext);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1205, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1285, 47, false);
#line 46 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.EmpFirstName));

#line default
#line hidden
            EndContext();
            BeginContext(1332, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1412, 45, false);
#line 49 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.EmpMidName));

#line default
#line hidden
            EndContext();
            BeginContext(1457, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1537, 46, false);
#line 52 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.EmpLastName));

#line default
#line hidden
            EndContext();
            BeginContext(1583, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1663, 41, false);
#line 55 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Gender));

#line default
#line hidden
            EndContext();
            BeginContext(1704, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1784, 41, false);
#line 58 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Salary));

#line default
#line hidden
            EndContext();
            BeginContext(1825, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(1905, 45, false);
#line 61 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Experience));

#line default
#line hidden
            EndContext();
            BeginContext(1950, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2030, 38, false);
#line 64 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.DOB));

#line default
#line hidden
            EndContext();
            BeginContext(2068, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2148, 43, false);
#line 67 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.DisplayFor(modelItem => item.DeptName));

#line default
#line hidden
            EndContext();
            BeginContext(2191, 79, true);
            WriteLiteral("\r\n                    </td>\r\n                    <td>\r\n                        ");
            EndContext();
            BeginContext(2271, 89, false);
#line 70 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.ActionLink("Edit", "EditEmp", new { id1 = item.UserId }, new { id2 = item.AdminId }));

#line default
#line hidden
            EndContext();
            BeginContext(2360, 28, true);
            WriteLiteral(" |\r\n                        ");
            EndContext();
            BeginContext(2389, 90, false);
#line 71 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
                   Write(Html.ActionLink("Delete", "Delete", new { id1 = item.UserId }, new { id2 = item.AdminId }));

#line default
#line hidden
            EndContext();
            BeginContext(2479, 52, true);
            WriteLiteral("\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 74 "C:\Users\sagnik.r\source\repos\TrialProject\TrialProject\Views\Admin\Data.cshtml"
            }

#line default
#line hidden
            BeginContext(2546, 727, true);
            WriteLiteral(@"        </tbody>
    </table>
</div>

<link href=""//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"" rel=""stylesheet"" />
<script src=""//code.jquery.com/jquery-3.4.1.min.js""></script>

<script src=""//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js""></script>

<script>
    $(document).ready(function () {
        $(""#Employees"").DataTable(
            {
                ""columnDefs"": [
                    { ""targets"": [0], ""visible"": false, ""searchable"": false },
                    { ""targets"": [1], ""searchable"": false, ""sortable"": false },
                    { ""targets"": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], ""autoWidth"": true }
                ]
            }
        );
    });
</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<TrialProject.Models.ViewModelAdmin>> Html { get; private set; }
    }
}
#pragma warning restore 1591
